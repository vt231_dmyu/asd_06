#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <iostream>
#include <chrono>
#define GETTIME std::chrono::steady_clock::now
#define CALCTIME std::chrono::duration_cast<std::chrono::nanoseconds>

void generateRandomData(int arr[], int size, int max, int min = 0) {
	for (int i = 0; i < size; i++)
		arr[i] = min + rand() % (max - min + 1);
}

void shellSort(int arr[], int n) {
	int s = 0;
	// ���������� ������� �������� �� ����� ������� ��������, ���� �� ��������� ������ ��������, ��� ����� �������� ���� pow(2, s) - 1 �������� ����� ������ n
	while (pow(2, s) - 1 <= n)
		s++;

	int step = pow(2, s) - 1;

	// �������� �������� �� ������ ����������
	while (s >= 1) {
		// ��������� ���������� ��������� ��� �������� � �������� ������
		for (int i = 0; i < n - step; i++) {
			// ������� ������� ������� �������
			for (int j = i; j >= 0 && arr[j] > arr[j + step]; j -= step) {
				int temp = arr[j];
				arr[j] = arr[j + step];
				arr[j + step] = temp;
			}
		}
		s--;
		step = pow(2, s) - 1;
	}
}

void printArray(int arr[], int n)
{
	for (int i = 0; i < n; i++)
		printf("%d ", arr[i]);
	printf("\n");
}

int main() {
	system("chcp 1251"); // ������� � ������ �� ��������
	system("cls"); // ������� ���� ������
	srand(time(NULL));

	int sizes[] = { 10, 100, 500, 1000, 2000, 5000, 10000 };
	//int sizes[] = { 10, 100 };
	int s, n = sizeof(sizes) / sizeof(sizes[0]);

	for (int i = 0; i < n; i++) {
		s = sizes[i];
		int* arr = (int*)malloc(sizeof(int) * s);
		generateRandomData(arr, s, 400);

		//printArray(arr, s);

		auto begin = GETTIME();
		shellSort(arr, s);
		auto end = GETTIME();
		auto spent = CALCTIME(end - begin);

		//printArray(arr, s);

		printf("��� ���������� �� ���������� ������ � %d �������� ������� �����: %lld ��.\n", s, spent.count());
		//printf("\n");
		free(arr);
	}
	printf("\n");

	return 0;
}
