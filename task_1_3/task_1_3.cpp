#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <iostream>
#include <chrono>
#define GETTIME std::chrono::steady_clock::now
#define CALCTIME std::chrono::duration_cast<std::chrono::nanoseconds>
#define RANGE 111 // ����������� �������� + 1 (�� -10 �� 100)

void generateRandomData(short arr[], int size, int max, int min = 0) {
    for (int i = 0; i < size; i++)
        arr[i] = min + rand() % (max - min + 1);
}

void countingSort(short arr[], int n) {
    short* output = (short*)malloc(sizeof(short) * n);
    int count[RANGE] = { 0 };

    // ϳ�������� ������� ������� �������� � ����� arr
    for (int i = 0; i < n; ++i)
        ++count[arr[i] + 10]; // ��������� ��'���� �������� ������

    // ϳ�������� ������� ��������, �� ����� ��� ���� ��������� ��������
    for (int i = 1; i < RANGE; ++i)
        count[i] += count[i - 1];

    for (int i = n - 1; i >= 0; --i) {
        int j = count[arr[i] + 10] - 1; // �������� ��������� ������� ��� ��������� ��������
        output[j] = arr[i]; // �������� �������� ������� � �������� ����� �� ���� ��������� �������
        --count[arr[i] + 10]; // �������� ������� ��������, �� ���������� �� �������
    }

    for (int i = 0; i < n; ++i)
        arr[i] = output[i];
    free(output);
}

void printArray(short arr[], int n)
{
    for (int i = 0; i < n; i++)
        printf("%d ", arr[i]);
    printf("\n");
}

int main() {
    system("chcp 1251"); // ������� � ������ �� ��������
    system("cls"); // ������� ���� ������
    srand(time(NULL));

    int sizes[] = { 10, 100, 500, 1000, 2000, 5000, 10000 };
    //int sizes[] = { 10, 100 };
    int s, n = sizeof(sizes) / sizeof(sizes[0]);

    for (int i = 0; i < n; i++) {
        s = sizes[i];
        short* arr = (short*)malloc(sizeof(short) * s);
        generateRandomData(arr, s, 100, -10);

        //printArray(arr, s);

        auto begin = GETTIME();
        countingSort(arr, s);
        auto end = GETTIME();
        auto spent = CALCTIME(end - begin);

        //printArray(arr, s);

        printf("��� ���������� �� ���������� ������ � %d �������� ����������: %lld ��.\n", s, spent.count());
        //printf("\n");
        free(arr);
    }
    printf("\n");

    return 0;
}
