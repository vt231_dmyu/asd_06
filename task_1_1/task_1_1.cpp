#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <iostream>
#include <chrono>
#define GETTIME std::chrono::steady_clock::now
#define CALCTIME std::chrono::duration_cast<std::chrono::nanoseconds>

void swap(float* a, float* b)
{
    float temp = *a;
    *a = *b;
    *b = temp;
}

void heapify(float arr[], int N, int i)
{
    // ����������� largest �� �����
    int largest = i;
    int left = 2 * i + 1;
    int right = 2 * i + 2;

    // ���� ���� �������� ������� ������ �� �����
    if (left < N && arr[left] > arr[largest])
        largest = left;

    // ���� ������ �������� ������� ������ �� �����
    if (right < N && arr[right] > arr[largest])
        largest = right;

    // ���� ��������� ������� �� �����
    if (largest != i) {
        swap(&arr[i], &arr[largest]);
        heapify(arr, N, largest);
    }
}

void heapSort(float arr[], int N)
{
    // ������ max-����
    for (int i = N / 2 - 1; i >= 0; i--)
        heapify(arr, N, i);
    // ϳ��������� ����������
    for (int i = N - 1; i >= 0; i--) {
        // ���������� ����� � ����� ����
        swap(&arr[0], &arr[i]);
        // ��������� �-��� heapify ��� ���������� ���� �� max-����
        heapify(arr, i, 0);
    }
}

void printArray(float arr[], int n)
{
    for (int i = 0; i < n; i++)
        printf("%.2f ", arr[i]);
    printf("\n");
}

int main() {
    system("chcp 1251"); // ������� � ������ �� ��������
    system("cls"); // ������� ���� ������
    srand(time(NULL));

    int sizes[] = { 10, 100, 500, 1000, 2000, 5000, 10000 };
    //int sizes[] = { 10, 100 };
    int s, n = sizeof(sizes) / sizeof(sizes[0]);

    for (int i = 0; i < n; i++) {
        s = sizes[i];
        float* arr = (float*)malloc(sizeof(float) * s);
        for (int i = 0; i < s; i++)
            arr[i] = (-10000 + rand() % (1000 - (-10000) + 1)) / 100.0;

        //printArray(arr, s);

        auto begin = GETTIME();
        heapSort(arr, s);
        auto end = GETTIME();
        auto spent = CALCTIME(end - begin);

        //printArray(arr, s);

        printf("��� ���������� �� ���������� ���������� ������ � %d ��������: %lld ��.\n", s, spent.count());
        //printf("\n");
        free(arr);
    }
    printf("\n");

    return 0;
}
